package com.ferit.landekam.rma_dz1_halloffame;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityMain extends AppCompatActivity {

    TextView tvNamePetrovic, tvYearsPetrovic, tvDescriptionPetrovic,
             tvNameOneal, tvYearsOneal, tvDescriptionOneal,
             tvNameFilipovic, tvYearsFilipovic, tvDescriptionFilipovic;
    ImageView ivPetrovic, ivOneal, ivFilipovic;


    HallOfFame Petrovic = new HallOfFame("Dražen Petrović","1964. - 1993.",
            "Dražen Petrović, bio je hrvatski košarkaš. Bio je jedan od najvećih hrvatskih i svjetskih košarkaša. Smatra se predvodnikom vala europskih košarkaša u NBA.");
    HallOfFame Oneal = new HallOfFame("Shaquille O'Neal","1972. - ",
            "Shaquille Rashaun O'Neal umirovljeni je američki profesionalni košarkaš. Igrao je na poziciji centra. Osvojio je 4 NBA prstena.");
    HallOfFame Filipovic = new HallOfFame("Mirko Filipović","1974. - ",
            "Mirko 'Cro Cop' Filipović je bivši borac mješovitih borilačkih vještina, hrvatski boksač, kickboksač i K-1 borac. Ima 38 pobjeda, 11 poraza, 2 neodlučene i 1 nevažeću borbu.");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }

    private void initializeUI() {
        this.tvNamePetrovic = findViewById(R.id.TVNamePetrovic);
        this.tvYearsPetrovic = findViewById(R.id.TVYearsPetrovic);
        this.tvDescriptionPetrovic = findViewById(R.id.TVDescriptionPetrovic);
        this.tvNameOneal = findViewById(R.id.TVNameOneal);
        this.tvYearsOneal = findViewById(R.id.TVYearsOneal);
        this.tvDescriptionOneal = findViewById(R.id.TVDescriptionOneal);
        this.tvNameFilipovic = findViewById(R.id.TVNameFilipovic);
        this.tvYearsFilipovic = findViewById(R.id.TVYearsFilipovic);
        this.tvDescriptionFilipovic = findViewById(R.id.TVDescriptionFilipovic);

        this.ivPetrovic = findViewById(R.id.IVPetrovic);
        this.ivOneal = findViewById(R.id.IVOneal);
        this.ivFilipovic = findViewById(R.id.IVFilipovic);

        this.tvNamePetrovic.setText(Petrovic.GetName());
        this.tvYearsPetrovic.setText(Petrovic.GetYears());
        this.tvDescriptionPetrovic.setText(Petrovic.GetDescription());
        this.tvNameOneal.setText(Oneal.GetName());
        this.tvYearsOneal.setText(Oneal.GetYears());
        this.tvDescriptionOneal.setText(Oneal.GetDescription());
        this.tvNameFilipovic.setText(Filipovic.GetName());
        this.tvYearsFilipovic.setText(Filipovic.GetYears());
        this.tvDescriptionFilipovic.setText(Filipovic.GetDescription());

        ivPetrovic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ActivityMain.this, "New Jersey Nets", Toast.LENGTH_SHORT).show();
            }
        });
        ivOneal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ActivityMain.this, "Los Angeles Lakers", Toast.LENGTH_SHORT).show();
            }
        });
        ivFilipovic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ActivityMain.this, "K1", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

package com.ferit.landekam.rma_dz1_halloffame;

public class HallOfFame {
    private String Name, YearsOfBirthAndDeath, Description;

    public HallOfFame(String name, String yearsOfBirthAndDeath, String description){
        Name = name;
        YearsOfBirthAndDeath = yearsOfBirthAndDeath;
        Description = description;
    }

    public void SetName(String name){
        Name = name;
    }

    public void SetYears(String years){
        YearsOfBirthAndDeath = years;
    }

    public void SetDescription(String description){
        Description = description;
    }

    public String GetName(){
        return Name;
    }

    public String GetYears(){
        return YearsOfBirthAndDeath;
    }

    public String GetDescription(){
        return Description;
    }
}
